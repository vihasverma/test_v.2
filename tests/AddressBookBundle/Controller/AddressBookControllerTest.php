<?php
namespace tests\PhpunitBundle\Controller;

use AddressBookBundle\Controller\AddressBookController;
use AddressBookBundle\Services\AddressBookService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;

class AddressBookControllerTest extends TestCase
{
    private $AddressBookServiceMock;
    private $addressBookControllerMock;
    private $twig;
    private $route;

    protected function setUp()
    {

        $this->twig = $this->getMockBuilder(\Twig_Environment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->route = $this->getMockBuilder(RouterInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->AddressBookServiceMock = $this->getMockBuilder(AddressBookService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->addressBookControllerMock = new AddressBookController(
            $this->AddressBookServiceMock, $this->twig, $this->route
        );
    }

    protected function tearDown()
    {
        $this->AddressBookServiceMock = null;
        $this->addressBookControllerMock = null;
    }

    public function testGetAllAction()
    {
        $this->AddressBookServiceMock
            ->expects($this->once())
            ->method('getAll');

        $this->addressBookControllerMock->getAllAction();
    }


    public function testCreate()
    {
        $this->AddressBookServiceMock
            ->expects($this->once())
            ->method('create');
        $this->route
            ->expects($this->once())
            ->method('generate')
            ->willReturn('list');
        $this->addressBookControllerMock->createAction();

    }

    public function testUpdateOneAction()
    {
        $this->AddressBookServiceMock
            ->expects($this->once())
            ->method('edit');
        $this->route
            ->expects($this->once())
            ->method('generate')
            ->willReturn('list');
        $this->addressBookControllerMock->editAction(1);
    }

    public function testDeleteOneAction()
    {
        $this->AddressBookServiceMock
            ->expects($this->once())
            ->method('delete');

        $this->route
            ->expects($this->once())
            ->method('generate')
            ->willReturn('list');

        $this->addressBookControllerMock->deleteAction(1);
    }


}