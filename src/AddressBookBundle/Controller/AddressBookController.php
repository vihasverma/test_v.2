<?php

namespace AddressBookBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AddressBookBundle\Services\AddressBookService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * @Route("/addressbook", service="controller.addressbook")
 */
class AddressBookController
{
    private $AddressBookService;
    private $twig;
    private $router;
    public function __construct(
        AddressBookService $AddressBookService,
        Environment $twig,
        RouterInterface $router
    )
    {
        $this->AddressBookService = $AddressBookService;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @Route("/", name="list")
     */
    public function getAllAction()
    {
        $result = $this->AddressBookService->getAll();
        $content = $this->twig->render('@AddressBook/list.html.twig', [
            'addressbook' => $result]);
        return new Response($content);

    }

    /**
     *
     * @Route("/create", name="create")
     */
    public function createAction()
    {
        $form = $this->AddressBookService->create();
        if (is_object($form)) {
            $content = $this->twig->render('@AddressBook/create.html.twig', [
                'form' => $form
            ]);
            return new Response($content);
        }
        return new RedirectResponse($this->router->generate('list'));
    }

    /**
     *
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction($id)
    {
        $form = $this->AddressBookService->edit($id);
        if (is_object($form)) {
            $content = $this->twig->render('@AddressBook/create.html.twig', [
                'form' => $form
            ]);
            return new Response($content);
        }

        return new RedirectResponse($this->router->generate('list'));
    }

    /**
     *
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {
        $this->AddressBookService->delete($id);

        return new RedirectResponse($this->router->generate('list'));
    }
}