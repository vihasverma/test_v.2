<?php
namespace AddressBookBundle\Services;

use AddressBookBundle\Entity\AddressBookRepository;
use AddressBookBundle\Form\Type\AddressType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class AddressBookService
{
    private $addressBookRepo;
    private $form;
    private $request;
    public function __construct(AddressBookRepository $addressBookRepository,
                                FormFactory $form,
                                RequestStack $requestStack)
    {
        $this->addressBookRepo = $addressBookRepository;
        $this->form = $form;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getAll()
    {
        return $this->addressBookRepo->findAll();
    }

    public function create()
    {
        $form = $this->form->create(AddressType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->getData();
            $picture = $address->getPicture();
            if ($picture != "") {
                $newFileName = $this->generateUniqueFileName() . "." . $picture->guessExtension();
                $address->setPicture($newFileName);
                $picture->move("../web/pictures", $newFileName);
            }else{
                $address->setPicture('');
            }
            $this->addressBookRepo->save($address);
            return true;
        }
        return $form->createView();
    }

    public function edit($id)
    {
        $addressEdit = $this->addressBookRepo->findOneBy(array("id" => $id));
        $form = $this->form->create(AddressType::class, $addressEdit);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->getData();
            $picture = $address->getPicture();
            if ($picture != "") {
                $newFileName = $this->generateUniqueFileName() . "." . $picture->guessExtension();
                $address->setPicture($newFileName);
                $picture->move("../web/pictures", $newFileName);
            }else{
                $address->setPicture('');
            }
            $this->addressBookRepo->save($address);
            return true;
        }

        return $form->createView();
    }

    public function delete($id)
    {
        $addressDelete = $this->addressBookRepo->findOneBy(array("id" => $id));
        $this->addressBookRepo->delete($addressDelete);
        return true;
    }

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}