LillyDoo Challenge
===============



## The address book containing the following data (Add/Edit/Delete/Listing):


    Firstname
    Lastname
    Street and number
    Zip
    City
    Country
    Phonenumber
    Birthday
    Email address
    Picture (optional)



##Techniques:

    Symfony 3.4
    Doctrine with SQLite
    Twig
    PHP 7.0



### Installation


Install the dependencies and start the server.

```sh
$ git clone https://vihasverma@bitbucket.org/vihasverma/test_v.2.git
$ cd test_v.2
$ composer install
move the db.sqlite file from root to /var/data/ or the folder mention in composer install step
$ php bin/console server:run
```

##URL for addressbook: (/)

http://127.0.0.1:8000/addressbook/


### Database

var\data\db.sqlite



### Code Structure and Design 
Code has been created in symfony 2 framework. Database ORM used is doctrine. 
I have done most of the coding in new bundle "AddressBookBundle"
Form Builder is used to create forms. Twig is used to keep the design seperate.

Entity: AddressBundle\Entity\AddressBook
Doctine migration file: Version20190527205041
Service File: AddressBookService.php


I have tried to use controller as service as a best practice and make use of service class.


### Unit Tets

Some phpunit test are also written for controller (AddressBookControllerTest.php)
